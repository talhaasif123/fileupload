package com.devfast.myapplication;

public class URLs {
    private static final String ROOT_URL = "https://cut-off-logic.000webhostapp.com/AndroidPdfUpload/Api.php?apicall=";

    public static final String URL_REGISTER = ROOT_URL + "signup";
    public static final String URL_LOGIN = ROOT_URL + "login";
}
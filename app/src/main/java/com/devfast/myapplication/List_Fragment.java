package com.devfast.myapplication;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static android.content.Context.MODE_PRIVATE;


public class List_Fragment extends Fragment {

    private View view;

    public static final String PDF_FETCH_URL = "https://cut-off-logic.000webhostapp.com/AndroidPdfUpload/getPdfs.php";
    public static final String PDF_DELETE_URL = "https://cut-off-logic.000webhostapp.com/AndroidPdfUpload/delete.php";
    ListView listView;
    ArrayList<Pdf> pdfList = new ArrayList<Pdf>();
    SwipeRefreshLayout pullToRefresh;
    //pdf adapter
//    ProgressDialog progressDialog;
    PdfAdapter pdfAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.list_layout, container, false);
        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // progressDialog = new ProgressDialog(getContext());
        listView = view.findViewById(R.id.listView);
        getPdfs();
        pullToRefresh = view.findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Here you can update your data from internet or from local SQLite data
                getPdfs();
                //         pdfAdapter.notifyDataSetChanged();
                pullToRefresh.setRefreshing(false);
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, final View view, final int position, long id) {


                ImageView deleteButton = (ImageView) view.findViewById(R.id.btnDelete);
                ImageView mainImage = (ImageView) view.findViewById(R.id.mainImage);
                TextView FileUrl = (TextView) view.findViewById(R.id.textViewUrl);
                TextView FileName = (TextView) view.findViewById(R.id.textViewName);
                deleteButton.setTag(position);

                deleteButton.setOnClickListener(
                        new Button.OnClickListener() {
                            @Override
                            public void onClick(View v) {


                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
                                alertDialogBuilder.setMessage("Are you sure want to Delete ?");
                                alertDialogBuilder.setPositiveButton("Yes",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface arg0, int arg1) {


                                                deleteFile(pdfList.get(position).getUrl());
                                              //  Toast.makeText(getActivity(),deleteFile(pdfList.get(position).getUrl()),Toast.LENGTH_LONG).show();
                                                pdfList.remove(position);
                                                pdfAdapter.notifyDataSetChanged();
                                            }
                                        });

                                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // finish();
                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();
                                alertDialog.setCancelable(false);


                            }
                        });
                mainImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openDoc(parent, position);
                    }
                });
                FileName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        openDoc(parent, position);
                    }
                });
                FileUrl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        openDoc(parent, position);
                    }
                });

            }
        });
    }

    private String deleteFile(String url) {
        RequestHandler requestHandler = new RequestHandler();

        //creating request parameters
        HashMap<String, String> params = new HashMap<>();
        params.put("url", url);

        //returing the response
        return requestHandler.sendPostRequest(PDF_DELETE_URL, params);


    }

    private void openDoc(final AdapterView<?> parent, int position) {
        Pdf pdf = (Pdf) parent.getItemAtPosition(position);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);


        String text = String.valueOf(Uri.parse("https://cut-off-logic.000webhostapp.com/AndroidPdfUpload/uploads/" + Uri.parse(pdf.getUrl())));

        String[] token = text.split("/");
        String pdfName = token[token.length - 1].trim();

        intent.setDataAndType(Uri.parse("https://cut-off-logic.000webhostapp.com/AndroidPdfUpload/uploads/" + pdfName), "application/pdf");
        startActivity(intent);
    }

    private void getPdfs() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, PDF_FETCH_URL,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (pdfList.size() > 0) {
                            pdfAdapter.clear();
                            pdfAdapter.notifyDataSetChanged();
                        }
                        try {
                            JSONObject obj = new JSONObject(response);
                            Toast.makeText(getContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                            JSONArray jsonArray = obj.getJSONArray("files");

                            for (int i = 0; i < jsonArray.length(); i++) {

                                //Declaring a json object corresponding to every pdf object in our json Array
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                //Declaring a Pdf object to add it to the ArrayList  pdfList
                                Pdf pdf = new Pdf();
                                String pdfName = jsonObject.getString("name");
                                String pdfUrl = jsonObject.getString("url");
                                pdf.setName(pdfName);
                                pdf.setUrl(pdfUrl);
                                pdfList.add(pdf);

                            }
                            SharedPreferences prefs = getContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                            String restoredText = prefs.getString("type", null);

                            if(restoredText.equals("Admin")) {
                                pdfAdapter = new PdfAdapter(getActivity(), R.layout.list_layout1, pdfList);
                            }
                            else
                            {
                                pdfAdapter = new PdfAdapter(getActivity(), R.layout.list_layout2, pdfList);

                            }
                            listView.setAdapter(pdfAdapter);

                            pdfAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), "No Response", Toast.LENGTH_LONG).show();
                    }
                }
        );

        RequestQueue request = Volley.newRequestQueue(getContext());
        request.add(stringRequest);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.options1, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                logout();
                return true;
        }

        return (super.onOptionsItemSelected(item));
    }


    public void logout() {


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        alertDialogBuilder.setMessage("Are you sure want to Logout ?");
        alertDialogBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {


                        SharedPreferences sharedPreferences = getContext().getSharedPreferences("simplifiedcodingsharedpref", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.clear();
                        editor.apply();
                        getContext().startActivity(new Intent(getContext(), LoginActivity.class));
                        getActivity().finish();

                    }
                });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        alertDialog.setCancelable(false);



    }


}




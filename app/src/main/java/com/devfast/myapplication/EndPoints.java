package com.devfast.myapplication;

/**
 * Created by Belal on 10/24/2017.
 */




public class EndPoints {
    private static final String ROOT_URL = "https://cut-off-logic.000webhostapp.com/api.php?apicall=";
    public static final String UPLOAD_URL = ROOT_URL + "uploads";
    public static final String GET_PICS_URL = ROOT_URL + "getpdf";
}
